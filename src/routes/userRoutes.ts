'use strict'

import express from 'express'
import {
  getUser,
  createUser,
  getAllUsers,
} from '../controllers/users.controller'

const userRouter = express.Router()

userRouter.get('/users/all', getAllUsers)
userRouter.post('/create-a-user', createUser)
userRouter.get('/users', getUser)

export default userRouter
