'use strict'

import { Sequelize } from 'sequelize'
import dotenv from 'dotenv'

if (process.env.NODE_ENV !== 'production') {
  dotenv.config()
}

const DatabaseConnection = new Sequelize(
  process.env.db_name,
  process.env.db_username,
  process.env.db_password,
  {
    host: 'localhost',
    dialect: 'mariadb',
    dialectOptions: {
      timezone: process.env.db_timezone || 'Etc/GMT0',
    },
    pool: {
      min: 0,
      max: 5,
      idle: 10000,
    },
    define: {
      charset: 'utf8',
      timestamps: false,
    },
    benchmark: false,
    logging: console.log,
    ssl: true,
  },
)
export default DatabaseConnection
