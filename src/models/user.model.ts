'use strict'

import DatabaseConnection from '../config/databaseConnection'
import { DataTypes, Sequelize, UUIDV4 } from 'sequelize'

export const User = DatabaseConnection.define('User', {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  uuid: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
  },
})

DatabaseConnection.sync({
  force: false,
})
