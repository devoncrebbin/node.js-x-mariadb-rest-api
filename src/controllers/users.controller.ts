'use strict'

import { User } from '../models/user.model'

export const getAllUsers = async function (req: any, res: any) {
  try {
    const user = await User.findAll()
    res.send(user)
  } catch (error) {
    res.status(500).send()
    const referenceError: ReferenceError = error
    console.error(referenceError.stack)
  }
}

export const createUser = async function (req: any, res: any) {
  try {
    const user = await User.create({
      name: req.body.name,
    })
    res.status(201).send(user)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const getUser = async function (req: any, res: any) {
  try {
    const user = await User.findOne(req.query.id)
    res.send(user)
  } catch (error) {
    res.status(500).send()
  }
}
