'use strict'

import express from 'express'
import userRouter from './routes/userRoutes'

const app = express()
app.get('/', (req: any, res: any) => {
  return res.send('Api Working')
})
app.use(userRouter)

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

app.listen(`${process.env.port}`, () => {
  console.log(`Listening on port ${process.env.port}`)
})

export default app
